#!/usr/bin/python

# script for generating a list of commands. saved in commands.lst
import os
import socket

if socket.gethostname() == "v570":
    laptop = True
else:
    laptop = False

outfile = 'commands.lst'


# studying the impact of clause learning strategies
def cl_strategy():
    prefix = 'python runOneLocal.py'
    binary = 'minisat2.2-hyper'
    workDir = "TD"
    if laptop:
        instFile = "/home/chenwx/workspace/satcode/hyperplane_reduction/2014.indu.laptop"
        rootDir = "/home/chenwx/workspace"
    else:
        instFile = "/s/chopin/b/grad/chenwx/sched/hyperplane_reduction/2014.indu.desk"
        rootDir = "/s/chopin/b/grad/chenwx/sched"
    fullWorkDir = "{}/{}".format(rootDir, workDir)

    # parameters to MS
    clauseLearntMethodRange = [0, 1, 2, 3]  # default to 0
    learntSizePath = True
    reduceDBAnalysis = True

    with open(instFile, 'r') as f, open(outfile, 'w') as out:
        for inst in f:
            for clauseLearntMethod in clauseLearntMethodRange:
                sepInst = inst.split()
                if not sepInst: continue  # skip empty lines
                [name, fpath] = sepInst
                nameWithParam = "{}-CL{}".format(name, clauseLearntMethod)
                if laptop:
                    outputDir = "{}/new/{}-{}-CL{}".format(fullWorkDir, binary, name, clauseLearntMethod)
                else:
                    outputDir = "{}/new/{}-{}-CL{}".format(rootDir, binary, name, clauseLearntMethod)
                if not os.path.exists(outputDir):
                    up_cmd = 'cd {}; cp -r {} /tmp/{}; cd /tmp/{};'.format(rootDir, workDir, nameWithParam,
                                                                           nameWithParam)
                    run_cmd = '{} {} {} {} {} {} {} {};'.format(prefix, binary, name, fpath, fullWorkDir,
                                                                clauseLearntMethod, learntSizePath, reduceDBAnalysis)
                    down_cmd = 'rm -rf /tmp/{}; cd {};'.format(nameWithParam, fullWorkDir)
                    cmd = "{} {} {}".format(up_cmd, run_cmd, down_cmd)
                    print >> out, cmd
                else:
                    print outputDir, 'exist'
