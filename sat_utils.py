import math


class SAT:
    def __init__(self, inst_path=None):
        self.clauses = []
        self.cand_sols = []
        self.last_sol = []
        self.exist_clauses = []

        if inst_path:
            self.read_cnf(inst_path)

    # read from instance file
    def read_cnf(self, inst_path):
        with open(inst_path, 'r') as f:
            for line in f:
                if line[0] == 'p':
                    terms = line.split()
                    self.n = int(terms[2])
                    self.m = int(terms[3])
                elif line[0] != 'c':  # skip commented lines
                    terms = line.split()
                    terms = [int(t) for t in terms if t != '0']  # convert to int and remove the ending '0'
                    terms = sorted(terms, key=abs)
                    self.clauses.append(terms)

    # enumerate 2^n initial candidate solution
    def enum_all_cand_sols(self):
        for i in xrange(int(math.pow(2, self.n))):
            if not self.last_sol:
                self.last_sol = [-i for i in range(1, self.n + 1)]
            else:
                self.last_sol = self.next_binary_string(self.last_sol[:])
                self.cand_sols.append(self.last_sol)
        return self.cand_sols

    # advancing from rightmost bits
    @staticmethod
    def next_binary_string(last_sol):
        i = len(last_sol) - 1
        while i >= 0 and last_sol[i] > 0:
            last_sol[i] = - last_sol[i]
            i -= 1
        last_sol[i] = - last_sol[i]
        return last_sol

    # take a clause, eliminates candidate solutions by matching patterns
    def elim_cand_solution(self, clause):
        forbidden_pattern = [-i for i in clause]
        removed_sols = []
        i = 0
        while i < len(self.cand_sols):
            if set(forbidden_pattern) < set(self.cand_sols[i]):
                removed_sols.append(self.cand_sols[i])
                self.cand_sols.remove(self.cand_sols[i])
            else:
                i += 1
        return removed_sols

    def add_exist_clause(self, clause):
        self.exist_clauses.append(clause)

    # using a closed-form formula to compute
    def compute_elim_cand_solution(self, new_clause):
        potential_elim = math.pow(2, self.n - len(new_clause))
        for c in self.exist_clauses:
            # check for overlaps in variables
            unsign_c = [abs(i) for i in c]
            unsign_new_clause = [abs(i) for i in new_clause]
            unsign_overlap_size = len(set(unsign_c).intersection(set(unsign_new_clause)))
            if unsign_overlap_size:  # there is overlap
                # check if all the signs of overlapping variables are the same
                overlap_size = len(set(c).intersection(set(new_clause)))
                if overlap_size == unsign_overlap_size:  # all signs are the same
                    potential_elim -= math.pow(2, overlap_size + self.n - len(c) - len(new_clause))
                    print "overlap with same signs only"
                else:  # there are at least one flipped signs, does nothing
                    print "overlap with one flipped signs"
            else:  # no overlap
                print "no overlap"
                potential_elim -= math.pow(2, self.n - len(c) - len(new_clause))
        return int(potential_elim)
