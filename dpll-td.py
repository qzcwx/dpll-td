# read tree decomposition

from treelib import Node, Tree
import copy
import Queue
import random

td = Tree()
td.create_node(None, 'A',  data=[2,3,4,9])
td.create_node(None, 'D',  data=[1,2,3,4], parent='A')
td.create_node(None, 'B',  data=[3,4,6], parent='D')
td.create_node(None, 'clauses',  data=[1,2,7], parent='D')
td.create_node(None, 'E',  data=[2,3,5], parent='D')
td.create_node(None, 'F',  data=[3,5,8], parent='E')
td.show()
# print(td.to_json(True))

# orig
num_var = 9                                     
num_clause = 8
C = [[2, -3, 5], [2, 3, 9], [3, 4, -9], [-1, 2, 3], [1, 2, -4], [-3, 5, 8], [1, -2, 7], [3, -4, 6]]
I = []

# clauses = [[-4, 5], [2, 3, 4], [5,6]]
# I = [-2,-3]

Ei = td.root
G = dict()
N = [] 
random.seed(1)

def unit_prop(C, in_I):
    # return the modified clauses
    # queue for unit propagation
    I = copy.copy(in_I)
    # print 'unit start I', I
    while I:
        i = I[0]    # propagate each element over the entire instance
        I.pop(0)
        # print 'i',i

        # print 'before', clauses
        idx = 0
        while idx < len(C):
        # for c in C_copy:
            c = C[idx]
            # print 'c', c
            clause_sat = False
            for v in c:
                if i==v:
                    # clause is SAT
                    clause_sat = True
                    break
                elif i==-v:
                    # print 'before', new_clause
                    c.remove(v)
                    # print 'after', c
                    break
            if clause_sat:
                C.remove(c)
            else: # the clause is SAT, can be removed/ignored
                if not c:      # all variables are removed no way to SAT
                    return C
                else:
                    if len(c)==1 and c[0] not in I:
                        # print "append"
                        I.append(c[0])
                idx = idx + 1
        # print 'after', clauses
        # print 'unit after I', I
    return C

def clause_by_cluster(clause, n, T):
    # return the clauses belonging only to the cluster contains variable
    # set V, not its parent node
    cur_v = n.data
    out_clause = []
    if n.bpointer:
        parent_v = T[n.bpointer].data
    else:
        parent_v = []
    for c in clause:
        # removing sign
        c_abs = [abs(i) for i in c]
        if set(c_abs)<set(cur_v) and not set(c_abs)<set(parent_v):
            out_clause.append(c)
    return out_clause

def all_pair(n):
    # return all pairs of indices from [0, n-1]
    l = []
    for i in range(n-1):
        for j in range(i+1, n):
            l.append((i,j))
    return l

def find_ind_vars(node, T):
    # fnid v such that v \in Ei and Ej and Ek, where Ej and Ek are two children of Ei
    # Traverse down the tree
    # for a cluster Ei check all pair of its children find the intersection
    # if node.is_root():
    X = set()
    node_name_list = node.fpointer
    # print 'node_name_list', node_name_list
    if len(node_name_list)>1:          # at least one pair of children
        pairs = all_pair(len(node_name_list))
        for p in pairs:
            X = X.union(list(set(node.data).intersection(set(T.get_node(node_name_list[p[0]]).data)).intersection(set(T.get_node(node_name_list[p[1]]).data))))
    for n in node_name_list:
        x = find_ind_vars(T.get_node(n), T)
        # print node.identifier, n, x
        X = X.union(x)
    return X

# inits
X = find_ind_vars(td[Ei], td)
print 'X', X 

# print clauses
# print N

# print 'U', U

# S = td.get_node(Ei).fpointer
# print S

def DPLL_TD(C, I, Ei, G, N):
    I = sorted(I, key=abs)
    print 'start, Ei:', Ei
    print 'I', I
    C = unit_prop(C, I)
    # print 'start: I', I
    print 'clauses', C
    print 'G', G
    N = unit_prop(N, I)
    # print 'start: I', I
    # print 'N', N
    U = C + N
    if [] in U:
        # print "UNSAT"
        return False
    else:
        if not U:
            # print "SAT"
            return True
        else:
            if I:
                assign = set([abs(i) for i in I])
            else:
                assign = set()
            print '(clauses or N)[Ei]', clause_by_cluster(U, td[Ei], td)
            print '(X and Ei)', X.intersection(set(td.get_node(Ei).data).difference(assign))
            if not clause_by_cluster(U, td[Ei], td) and not X.intersection(set(td.get_node(Ei).data).difference(assign)):
                print 'both empty'
                sat = True
                S = td.get_node(Ei).fpointer
                while sat and S:
                    print 'while sat and S', S
                    Ej = random.choice(S)
                    S.remove(Ej)
                    # if exist g in G[Ej], exist eg extension of g on Ei and Ej, I[Ei and Ej] is a subset eg:
                    subset = False
                    if Ej in G:
                        print 'Ej in G', 'Ej:', Ej
                        for g in G[Ej]:
                            # check if g can cover I on Ei and Ej
                            inter = [i for i in I if abs(i) in set(td.get_node(Ei).data).intersection(set(td.get_node(Ei).data))]
                            for i in inter:
                                for ge in g:
                                    if abs(i)==ge and i!=ge:
                                        break
                                if not subset:
                                    break
                            if subset:
                                I = list(set(I).union(set(g)))
                                I = sorted(I, key=abs)   # make sure I is sorted
                                break
                    if not subset: # else
                        print 'G before DPLL_TD', G, 'Ej', Ej
                        sat = DPLL_TD(C, I, Ej, G, N)
                        print 'G  after DPLL_TD', G, 'Ej', Ej
                        if sat:
                            print 'g = G[Ej][-1]'
                            g = G[Ej][-1]   # last recorded g
                            print 'g', g
                            print 'before union I', I
                            I = list(set(I).union(set(g)))
                            I = sorted(I, key=abs)   # make sure I is sorted
                            print 'after union I', I
                        else:
                            print 'N.append'
                            n = [-i for i in I if abs(i) in set(td.get_node(Ei).data).intersection(set(td.get_node(Ej).data))]
                            N.append(n)
                if sat:
                    print 'if Ei not in G:'
                    if Ei not in G:
                        G[Ei] = []
                    if td.parent(Ei):
                        G[Ei].append([i for i in I if abs(i) in set(td.get_node(Ei).data).intersection(set(td.parent(Ei).data))])
                        print Ei, 'appended'

                print 'return SAT', sat, 'I', I
                return sat
            else:
                # print 'end 0 I', I
                # choose an unassigned variable v from Ei
                print 'assign'
                # print 'I', I
                Ei_var = set(td.get_node(Ei).data)
                # print 'Ei var', Ei_var
                # print 'assign', assign
                diff = list(Ei_var.difference(assign))
                if diff:
                    v = random.choice(diff)
                else:
                    exit(0)
                print 'v', v
                # print 'end I', I+[v]
                if DPLL_TD(C+[[v]], I+[v], Ei, G, N):
                    return True
                else:
                    return DPLL_TD(C+[[-v]], I+[-v], Ei, G, N)
                
SAT = DPLL_TD(C, I, Ei, G, N)
print 'SAT', SAT
print 'final I', I
print 'G', G
print 'N', N
