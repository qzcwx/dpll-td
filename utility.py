# common utility for writing experiment scripts
import socket


def is_on_laptop():
    return socket.gethostname() == "v570"
