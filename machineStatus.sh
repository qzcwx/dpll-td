#!/bin/bash

# check machine status 
# while read m; do
for m in `cat machines.lst`
do
    printf $m,
    ping -c1 -w1 -q $m > /dev/null
    if [ $? -eq 0 ]; then
        ssh  -o ConnectTimeout=3 $m "mpstat 1 2 | tail -1 | awk '{ print \$NF }'" 
    else
        printf "dead\n"
    fi
done
