#!/usr/bin/python

# script for generating a list of commands, preprocessing all instances in a given directory
import os

# constants
cmd_file = 'commands.lst'
binary = '/s/chopin/b/grad/chenwx/sched/TD/bin/minisat-core'


def preprocess_minisat():
    """using minisat to preprocess instances"""

    # configurable parameters
    resDir = '/s/chopin/m/proj/sched/chenwx/inst/sat14/crafted/SAT/pre-cnf'
    instFile = '/s/chopin/b/grad/chenwx/sched/inst/lists/2014.crafted.sat.desk'

    with open(instFile, 'r') as f, open(cmd_file, 'w') as out:
        for inst in f:
            sepInst = inst.split()
            if not sepInst: continue  # skip empty lines
            [name, fpath] = sepInst
            outfile = "{}/{}.pre.cnf".format(resDir, name)
            if not os.path.exists(outfile):
                print >> out, '{} {} -dimacs={}'.format(binary, fpath, outfile)
            else:
                print outfile, 'exist'


def hyper_preprocess_minisat():
    """ minisat preprocessing after assigning variables in hyper """
    instFile = '/s/chopin/b/grad/chenwx/sched/inst/lists/2014.crafted.indu.small.tw.n.pre.filtered.desk'
    # instFile = '/home/chenwx/workspace/inst/lists/2014.crafted.indu.small.tw.n.pre.flitered.laptop'
    with open(instFile, 'r') as f, open(cmd_file, 'w') as out:
        for inst in f:
            sepInst = inst.split()
            if not sepInst: continue  # skip empty lines
            [name, fpath] = sepInst

            if "crafted" in fpath:
                resDir = '/s/chopin/m/proj/sched/chenwx/inst/sat14/crafted/SAT/pre-simp-cnf'
            elif "app-pre" in fpath:
                resDir = "/s/chopin/m/proj/sched/chenwx/inst/sat14/app/pre-simp-cnf"

            outfile = "{}/{}.cnf".format(resDir, name)
            hyper_path = "/s/chopin/m/proj/sched/chenwx/max-fixed/{}.backbone".format(name)
            if not os.path.exists(outfile):
                print >> out, '{} {} -hyper-path={} -dimacs-vig={}'.format(binary, fpath, hyper_path, outfile)
            else:
                print outfile, 'exist'


hyper_preprocess_minisat()
