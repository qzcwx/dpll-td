# draw VIG for a list of instances

import os

from graph_tool.all import *


def vig_all():
    """ compute vig for a list of instances """

    instList = "/home/chenwx/workspace/inst/lists/2014.crafted.indu.small.tw.n.pre.decomp.laptop"
    resDir = './vig/'

    if not os.path.exists(resDir):
        os.makedirs(resDir)

    with open(instList, 'r') as f:
        for inst in f:
            [name, fpath] = inst.split()
            print name

            # check the existence of the output
            output_path = resDir + name + ".pdf"
            if os.path.exists(output_path):
                print output_path, "exists"
                continue

            g = Graph(directed=False)
            with open(fpath, 'r') as inf:
                for line in inf:
                    if line[0] == 'p':
                        terms = line.split()
                        n = int(terms[2])
                        print "n", n
                    if line[0] != 'c' and line[0] != '0' and line[0] != 'p':
                        terms = line.split()
                        if len(terms) == 0:
                            break
                        terms = terms[:-1]
                        absTerms = [abs(int(i)) - 1 for i in terms]
                        for i in xrange(len(absTerms)):
                            for j in xrange(i + 1, len(absTerms)):
                                g.add_edge(absTerms[i], absTerms[j])

            remove_parallel_edges(g)
            graph_draw(g, output=output_path)
            print "done"


def connected_compnents():
    """ compute connected components for a list of instances """
    
    instList = "/home/chenwx/workspace/inst/lists/2014.crafted.indu.small.tw.n.pre.decomp.laptop"
    out_path = "/home/chenwx/workspace/result-td/045796b-decomp-vig-pseudo-backbone/decomp-vig.comp"

    with open(instList, 'r') as f, open(out_path, 'w') as out_f:
        for inst in f:
            [name, fpath] = inst.split()
            print name
            g = Graph(directed=False)
            with open(fpath, 'r') as inf:
                for line in inf:
                    if line[0] == 'p':
                        terms = line.split()
                        n = int(terms[2])
                    if line[0] != 'c' and line[0] != '0' and line[0] != 'p':
                        terms = line.split()
                        if len(terms) == 0:
                            break
                        terms = terms[:-1]
                        absTerms = [abs(int(i)) - 1 for i in terms]
                        for i in xrange(len(absTerms)):
                            for j in xrange(i + 1, len(absTerms)):
                                g.add_edge(absTerms[i], absTerms[j])

            remove_parallel_edges(g)
            comp, hist = label_components(g)
            if len(comp.a):
                num_comp = max(comp.a) + 1
            else:
                num_comp = 0
            print >> out_f, "{} {}".format(name, num_comp)
