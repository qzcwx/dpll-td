# runOne.sh
# take parameter as input, and runs only one stance, and run them on local disk, i.e., /tmp/

import subprocess as sp
import os
import shutil
import time
import sys
import datetime
import socket

import psutil

limit = 5000


def bytes2human(n):
    symbols = ('K', 'M', 'G', 'T', 'P', 'E', 'Z', 'Y')
    prefix = {}
    for i, s in enumerate(symbols):
        prefix[s] = 1 << (i + 1) * 10
    for s in reversed(symbols):
        if n >= prefix[s]:
            value = float(n) / prefix[s]
            return '%.1f%s' % (value, s)
    return "%sB" % n


def runtimeInfo():
    return "c {}: CPU, {} * {}%; RAM available: {}/{}\n" \
        .format(socket.gethostname(), psutil.cpu_count(), psutil.cpu_percent(),
                bytes2human(psutil.virtual_memory().available), bytes2human(psutil.virtual_memory().total))


def flush(out, err, dirName):
    path = "{}/{}.cnf".format(dirName, dirName)
    # store stdout
    with open(path + ".out", 'w') as f:
        f.write(runtimeInfo())
        f.write(out)
    # store stderr
    with open(path + ".err", 'w') as f:
        f.write(err)

# turn a string to a boolean value
def strToBool(strIn):
  return strIn.lower() in ("yes", "true")

print 'argv', sys.argv
solver = sys.argv[1]
nickname = sys.argv[2]
origInstPath = sys.argv[3]
fullWorkDir = sys.argv[4]
clauseLearntMethod = sys.argv[5]
learntSizePath = strToBool(sys.argv[6])
reduceDBAnalysis = strToBool(sys.argv[7])

instDir = "{}-{}-CL{}".format(solver, nickname, clauseLearntMethod)
print 'solver:', solver, "nickname", nickname, "clauseLearntMethod", clauseLearntMethod, "learntSizePath", learntSizePath, "reduceDBAnalysis", reduceDBAnalysis

if learntSizePath:
    learntSizePath = "{}/{}.learntsize".format(instDir, instDir)
if reduceDBAnalysis:
    reduceDBAnalysis = "{}/{}.reduceDB".format(instDir, instDir)

nickInstName = nickname + '.cnf'  # nick instance name with ".cnf"
print str(datetime.datetime.now()), 'instance', instDir

# create directory for dumping out
if os.path.exists(instDir):
    shutil.rmtree(instDir)
os.makedirs(instDir)

# create an empty file for storing runtime
open('{}/{}.time'.format(instDir, instDir), 'a').close()

# copy
shutil.copy(origInstPath, nickInstName)

cmdVec = ['timeout', str(limit), 'bin/' + solver, nickInstName, "-learnt-clause-method=" + clauseLearntMethod]
if learntSizePath:
    cmdVec.append("-learnt-size-path=" + learntSizePath)
if reduceDBAnalysis:
    cmdVec.append("-reduceDB-analysis-path=" + reduceDBAnalysis)

start = time.time()
p = sp.Popen(cmdVec, stdout=sp.PIPE, stderr=sp.PIPE)

out, err = p.communicate()
t = time.time() - start

flush(out, err, instDir)
# print 'out', out
# print 'err', err

# append overall time to *.time
with open('{}/{}.time'.format(instDir, instDir), 'a') as timefile:
    print >> timefile, '{}'.format(t)

# 124 if COMMAND times out
# 125 if `timeout' itself fails
# 126 if COMMAND is found but cannot be invoked
# 127 if COMMAND cannot be found
# 137 if COMMAND is sent the KILL(9) signal (128+9)
# the exit status of COMMAND otherwise

print str(datetime.datetime.now()), 'return', p.returncode

# save return code in file
with open('{}/{}.ret'.format(instDir, instDir), 'w') as retfile:
    print >> retfile, p.returncode

# compressing results and remove instance
os.system("gzip -r " + instDir)
os.remove(nickInstName)

# make sure root result directory exists
resRootDir =  "/s/chopin/b/grad/chenwx/sched/new"
if not os.path.exists(resRootDir):
    os.makedirs(resRootDir)

# copy the result to a
instResDir = resRootDir + "/" + instDir
if os.path.exists(instResDir):
    shutil.rmtree(instResDir)
shutil.copytree(instDir, instResDir)
