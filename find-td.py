#!/bin/python
# finding TD by generating scripts (set "gen_cmd_list" to True) or run TD directly on the current machine

import os
import utility as util

__author__ = 'chenwx'

# configurable parameters
method = 1  # 1 means min degree
maxTime = 3600  # time out for TD
gen_cmd_list = True  # whether to generate cmd list or run the jobs directly
on_pre_inst = False  # whether to run TD on preprocessed instances
inst_class = "crafted"  # the instance class

if util.is_on_laptop():
    binary = "/home/chenwx/workspace/TreeDecomposer/bin/Release/treed"
else:
    binary = "/s/chopin/m/proj/sched/chenwx/TreeDecomposer/bin/Release/treed"
cmd_list_name = "commands.lst"
parentDir = '/home/chenwx/workspace/result-td/'
desk_res_dir = "/s/chopin/m/proj/sched/chenwx/tmp/"


def create_res_dir(version):
    if util.is_on_laptop():
        # create directory
        resDir = parentDir + version + '/'
        if not os.path.exists(resDir):
            os.makedirs(resDir)
        return resDir
    else:
        return desk_res_dir


# td for original instances of 2014 industrial instances
def td_for_inst_list():
    if util.is_on_laptop():
        if not on_pre_inst:
            inst_list = "/home/chenwx/workspace/inst/lists/2014.{}.sat.laptop".format(inst_class)
        else:
            inst_list = "/home/chenwx/workspace/inst/lists/2014.{}.sat.pre.laptop".format(inst_class)
    else:
        if not on_pre_inst:
            inst_list = "/s/chopin/m/proj/sched/chenwx/inst/lists/2014.{}.sat.desk".format(inst_class)
        else:
            inst_list = "/s/chopin/m/proj/sched/chenwx/inst/lists/2014.{}.sat.pre.desk".format(inst_class)

    # version = "b8bec69"  # git commit id for ~/workspace/TreeDecomposer/
    version = "new"
    res_dir = create_res_dir(version)

    with open(inst_list, 'r') as f, open(cmd_list_name, "w") as cmd_f:
        for inst in f:
            [name, path] = inst.split()
            out_path = res_dir + name

            # check on the existence of td file, because it is generated only when the operation is completely done
            if not on_pre_inst and os.path.isfile("{}.td".format(out_path)) \
                    or on_pre_inst and os.path.isfile("{}.pre.td".format(out_path)):
                print out_path, "exists"
                continue

            if not on_pre_inst:
                cmd = "timeout {} {} {} {} {}.td 1>{}.out 2>{}.err" \
                    .format(maxTime, binary, path, method, out_path, out_path, out_path)
            else:
                cmd = "timeout {} {} {} {} {}.pre.td 1>{}.pre.out 2>{}.pre.err" \
                    .format(maxTime, binary, path, method, out_path, out_path, out_path)

            if gen_cmd_list:  # export to file
                print >> cmd_f, cmd
            else:
                ret = os.system(cmd)  # run directly on this machine.  124: time out
                print 'ret', ret, '\n'


td_for_inst_list()
