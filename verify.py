# verify the computation of utility

import sat_utils

def add_clause(c):
    print "Current clause:", c
    sat.compute_elim_cand_solution(c)
    print "computed remove:\n", sat.compute_elim_cand_solution(c)
    print "remove", sat.elim_cand_solution(c)
    sat.add_exist_clause(c)
    print "current exist clause", sat.exist_clauses
    print "remaining candidate solutions", sat.cand_sols
    print

fname = "tmp/aaai.cnf"
sat = sat_utils.SAT(fname)
print "Clauses", sat.clauses
sat.enum_all_cand_sols()
for c in sat.clauses:
    add_clause(c)

c = [1]
add_clause(c)
c = [2, 3]
add_clause(c)
c = [2, -3]
add_clause(c)
