# generate one graph at time
# python graph-one.py [input.cnf] [output.pdf/png]
from graph_tool.all import *
import sys
import time

fpath = sys.argv[1]
outfile = sys.argv[2]


def draw_vig_group(fpath, outfile):
    g = Graph(directed=False)
    with open(fpath, 'r') as inf:
        for line in inf:
            if line[0] == 'p':
                terms = line.split()
                n = int(terms[2])
            if line[0] != 'c' and line[0] != '0' and line[0] != 'p':
                terms = line.split()
                if len(terms) == 0:
                    break
                terms = terms[:-1]
                absTerms = [abs(int(i)) - 1 for i in terms]
                for i in xrange(len(absTerms)):
                    for j in xrange(i + 1, len(absTerms)):
                        g.add_edge(absTerms[i], absTerms[j])

    label = g.new_vertex_property("int")
    g.vertex_properties["label"] = label
    for i in range(n):
        label[i] = i + 1
    remove_parallel_edges(g)

    pos = sfdp_layout(g)
    spins = community_structure(g, 10000, 20, t_range=(5, 0.1))

    graph_draw(g, pos=pos, vertex_text=g.vertex_properties['label'], vertex_fill_color=spins, vertex_shape=spins,
               vertex_font_size=5,
               output=outfile)
    # print "modularity", modularity(g, spins)
    # group = g.new_vertex_property("int")
    # g.vertex_properties["group"] = group
    #
    # nodeIdList = readTD(n)
    # for i in range(n):
    #     group[i] = nodeIdList[i]
    # pos = sfdp_layout(g, groups=g.vertex_properties['group'])
    # graph_draw(g, pos=pos, vertex_text=g.vertex_properties['label'], vertex_fill_color=g.vertex_properties['group'], vertex_shape=g.vertex_properties['group'], vertex_font_size=5, output=outfile)


# draw vig
def draw_vig(fpath, outfile):
    g = Graph(directed=False)
    with open(fpath, 'r') as inf:
        for line in inf:
            if line[0] == 'p':
                terms = line.split()
                n = int(terms[2])
            if line[0] != 'c' and line[0] != '0' and line[0] != 'p':
                terms = line.split()
                if len(terms) == 0:
                    break
                terms = terms[:-1]
                absTerms = [abs(int(i)) - 1 for i in terms]
                for i in xrange(len(absTerms)):
                    for j in xrange(i + 1, len(absTerms)):
                        g.add_edge(absTerms[i], absTerms[j])

    remove_parallel_edges(g)

    graph_draw(g, output=outfile)


# draw vig with label from cnf, support gap between variable indices
def draw_vig_label(fpath, outfile):
    import numpy
    g = Graph(directed=False)
    label = g.new_vertex_property("int")
    g.vertex_properties["label"] = label

    label_dict = dict()  # reverse map (label -> vertex_index)
    start = time.time()

    with open(fpath, 'r') as inf:
        for line in inf:
            if line[0] == 'p':
                terms = line.split()
                n = int(terms[2])
                print n
            if line[0] != 'c' and line[0] != '0' and line[0] != 'p':
                terms = line.split()
                if len(terms) == 0:
                    break
                terms = terms[:-1]
                absTerms = [abs(int(i)) - 1 for i in terms]
                for i in xrange(len(absTerms)):
                    for j in xrange(i + 1, len(absTerms)):
                        i_label = absTerms[i]
                        j_label = absTerms[j]
                        if i_label not in label_dict:
                            v_i = g.add_vertex()
                            label[v_i] = i_label
                            label_dict[i_label] = int(v_i)
                        else:
                            v_i = g.vertex(label_dict[i_label])
                        if j_label not in label_dict:
                            v_j = g.add_vertex()
                            label[v_j] = j_label
                            label_dict[j_label] = int(v_j)
                        else:
                            v_j = g.vertex(label_dict[j_label])

                        g.add_edge(v_i, v_j, add_missing=False)

    remove_parallel_edges(g)
    end = time.time()
    print end - start

    graph_draw(g, vertex_text=g.vertex_properties['label'], vertex_font_size=4, output=outfile)


def readTD(n):
    tdfile = "/home/chenwx/workspace/result-td/b8bec69/aes_24_4_5.td"

    parentList = []  #
    varsList = []  # list of list storing variables in each node
    nodeIdList = [None] * n
    with open(tdfile, 'r') as tdin:
        tdLines = tdin.readlines()
        for l in tdLines:
            if 'TreeWidth' in l:
                tw = int(l.split()[1])
            elif 'Parent' in l:
                parentList.append(int(l.strip().split()[2]))
            elif 'Var' in l:
                terms = l.strip().split()
                vars = [int(i) for i in terms[2:len(terms)]]
                varsList.append(vars)
                for v in vars:
                    nodeIdList[v] = cur_tree_node
            elif 'TreeNode' in l:
                cur_tree_node = int(l.strip().split()[2])

    print "tw", tw
    print "parentList", parentList
    print "varsList", varsList
    return nodeIdList


draw_vig(fpath, outfile)
