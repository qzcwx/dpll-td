#!/bin/bash

# upload TD/minisat repository
lftp desktop << EOF
cd sched/
mirror -R ~/workspace/TD -x tmp -x .git
mirror -R ~/workspace/minisat -x .git -x variant 
bye
EOF

# compile 
ssh eggs.cs.colostate.edu "cd /s/chopin/b/grad/chenwx/sched/minisat/simp; 
export MROOT=/s/chopin/m/proj/sched/chenwx/minisat; 
make clean; 
make r -j 10; 
mv minisat_release ../../TD/bin/minisat2.2-hyper;
cd /s/chopin/b/grad/chenwx/sched/TD;
./genScriptMS.py"
