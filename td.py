# implementing the entire treedecomposition algorithm from scratch

from treelib import Node, Tree
import numpy as np
import sys

if len(sys.argv)==1:
    print 'please provide origInstPath of instance'
    exit()

var_order = [9, 6, 7, 1, 2, 4, 8, 3, 5]
# clauses = [[2, -3, 5], [2, 3, 9], [3, 4, -9], [-1, 2, 3], [1, 2, -4], [-3, 5, 8], [1, -2, 7], [3, -4, 6]]
C = []
tree = Tree()
new_clause = dict()
edge = dict()                             # maps parent node to children node
nodes = dict()
node_name = 1

# read from instance file
# inst_path = 'paper.cnf'
inst_path = sys.argv[1]
with open(inst_path, 'r') as f:
    for line in f:
        if line[0]=='p':
            terms = line.split()
            n = int(terms[2])
            m = int(terms[3])
        elif line[0]!='c':   # skip commented lines
            terms = line.split()
            terms = [int(t)  for t in terms if t!='0'] # convert to int and remove the ending '0'
            C.append(terms)

# print n, m
var_order = np.random.permutation(n)+1
# print 'clauses', clauses
# print 'var_order', var_order


for v in var_order:
    # print 'v', v
    # construct the new tree node
    node = set()
    found = False
    # scan the original clauses
    idx = 0
    while idx < len(C):
        c = C[idx]
        abs_c = [abs(i) for i in c]
        if v in abs_c:
            found = True
            node = node.union(set(abs_c))
            # print clauses
            # print 'remove', c
            C.remove(c)
            # print clauses
        else:
            idx = idx + 1
            
    if not found:
        continue
            
    # scan new clauses
    for key, value in new_clause.items():
        if v in value:
            found = True
            # delete clause and make edge
            del new_clause[key]
            edge[key] = node_name
            node = node.union(value)
            
    nodes[node_name] = node

    # add new clause associated with the added tree node
    # check if new clause cover any of the previous one
    one_new_clause = node.difference([v])
    for key, value in new_clause.items():
        if value.issubset(one_new_clause) or one_new_clause.issubset(value):
            del new_clause[key]
            edge[key] = node_name
    new_clause[node_name] = one_new_clause
    # print 'tree', tree
    # print 'clauses', len(clauses), clauses
    # print 'new_clause', new_clause
    # print 'nodes', nodes
    # print 'edge', edge
    # print 
    # node_name = chr(ord(node_name) + 1)
    node_name = node_name + 1
    
treewidth = max([len(i) for i in nodes.values()]) - 1
print 'treewidth', treewidth
