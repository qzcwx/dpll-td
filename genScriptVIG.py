#!/usr/bin/python

# script for generating a list of commands. saved in commands.lst
import re
import os
import shutil

binary = '/s/chopin/b/grad/chenwx/sched/TD/graph-one.py'
outfile_name = 'commands.lst'
resDir = '/s/chopin/b/grad/chenwx/sched/TD/vig'

if not os.path.exists(resDir):
    os.makedirs(resDir)

def gen_by_inst_list():
    instFile = '/s/chopin/m/proj/sched/chenwx/inst/lists/2014.crafted.sat.pre.desk'
    with open(instFile, 'r') as f, open(outfile_name, 'w') as out:
        for inst in f:
            sepInst = inst.split()
            if not sepInst: continue                      # skip empty lines
            [name, fpath] = sepInst
            outfile = "{}/{}.pdf".format(resDir, name)
            if not os.path.exists(outfile):
                print >>out, 'python {} {} {}'.format(binary, fpath, outfile)
            else:
                print outfile, 'exist'

def indu_2014_pre():
    inst_dir = "/s/chopin/b/grad/chenwx/sched/sat14/app-pre"
    inst_files = os.listdir(inst_dir)
    with open(outfile_name, 'w') as out:
        for inst in inst_files:
            name = re.search('(.+)\.cnf', inst).group(1)
            # print inst, name
            fpath = "{}/{}".format(inst_dir, inst)
            outfile = "{}/{}.pdf".format(resDir, name)
            if not os.path.exists(outfile):
                print >>out, 'python {} {} {}'.format(binary, fpath, outfile)
            else:
                print outfile, 'exist'

gen_by_inst_list()
